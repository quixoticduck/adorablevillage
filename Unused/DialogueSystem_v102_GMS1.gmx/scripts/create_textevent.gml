///create_textevent(text, speaker, *type, *next_line, *text_col, *emotion, *emote

if(instance_exists(obj_textevent)){ exit; }

var arg_count = argument_count;
var i = 0, var arg; repeat(arg_count){
	arg[i] = argument[i];
	i++;
}

var mono = instance_create(0,0,obj_textevent);

with(mono){
	switch(arg_count){
		case 7: myEmotes		= arg[6];
		case 6: myEmotions		= arg[5];
		case 5: myTextcol		= arg[4];
		case 4: myNextLines		= arg[3];
		case 3: myTypes			= arg[2];
	}
	mySpeakers	= arg[1];
	myText		= arg[0];
	
	event_perform(ev_other, ev_user0);
}

return mono;

