///create_dialogue(text, speaker, *type, *next_line, *text_col, *emotion, *emote


if(instance_exists(obj_textbox)){ exit; }

//Get Arguments
var arg = 0; var i = 0; var arg_count = argument_count;
repeat(arg_count){ arg[i] = argument[i]; i++; } 

//Setup Default Variables
var _type		= -1; 
var _nextline	= -1;
var _textcol	= -1;
var _emotion	= -1;
var _emotes		= -1;
var _creator	= id;

//Fill variables depending on argument count
switch(arg_count-1){
	case 6: _emotes		= arg[6];
	case 5: _emotion	= arg[5];
	case 4: _textcol	= arg[4];
	case 3: _nextline	= arg[3];
	case 2: _type		= arg[2];
	default:
		var text_		= arg[0];
		var _speaker	= arg[1];	
		var s_			= _speaker;	//for assigning _speaker during repeat loop
	break;
}

//If Text or Speaker aren't arrays (single line input), make them arrays 
if(is_array(text_))		{ var len = array_length_1d(text_); }
else					{ var len = 1; text_[0] = text_;  }
if(!is_array(_speaker))	{ 
	if(_speaker == -1)	{ s_ = id; }				//the default "-1" argument, ready for loop
	else				{ _speaker[0] = _speaker; }	//if we HAVE specified the speaker
}

//Fill arrays for default arguments
if(!is_array(_type)		and _type		== -1) { i = 0; repeat(len){ _type[i]		= 0;		i++; } }
if(!is_array(_nextline)	and _nextline	== -1) { i = 0; repeat(len){ _nextline[i]	= 0;		i++; } }
if(!is_array(_textcol)	and _textcol	== -1) { i = 0; repeat(len){ _textcol[i]	= c_white;	i++; } }
if(!is_array(_emotion)	and _emotion	== -1) { i = 0; repeat(len){ _emotion[i]	= 0;		i++; } }
if(!is_array(_emotes)	and _emotes		== -1) { i = 0; repeat(len){ _emotes[i]		= -1;		i++; } }
if(!is_array(_speaker)	and _speaker	== -1) { i = 0; repeat(len){ _speaker[i]	= s_;		i++; } }

//Create the Textbox
var textbox_ = instance_create(x,y, obj_textbox);
with(textbox_){
	creator		= _creator;
	type		= _type;
	text		= text_;
	nextline	= _nextline;
	textcol		= _textcol;
	emotion		= _emotion;	
	emotes		= _emotes;
	
	//Speaker's Variables
	i = 0; repeat(len){
		portrait[i] = _speaker[i].myPortrait;
		voice[i]	= _speaker[i].myVoice;
		font[i]		= _speaker[i].myFont;
		speaker[i]	= _speaker[i];		
		i++;
	}
	
	draw_set_font(font[0]);
	charSize = string_width("M");
	stringHeight = string_height("H");
	
	event_perform(ev_alarm, 0);
}

myTextbox = textbox_;
return textbox_;
